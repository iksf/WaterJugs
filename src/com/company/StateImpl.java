package com.company;

import cm3038.search.ActionStatePair;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StateImpl implements cm3038.search.State, Cloneable {
    /*A maximum value*/
    int a_max = 0;
    /*B maximum value*/
    int b_max = 0;
    /*Current A and B values*/
    int a = 0, b = 0;
    /*Target values*/
    int a_target = 0;
    int b_target = 0;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    StateImpl() {
    }

    /*Override hashcode method to generate unique number for each state */
    @Override
    public int hashCode() {
        return a * (a_max + b_max) + b;
    }

    /*Override equals method to override default behaviour of reference comparison*/
    @Override
    public boolean equals(Object o) {
        if (o != null)
            return this.hashCode() == o.hashCode();
        return false;
    }

    @Override
    public String toString() {
        return "A: " + this.a + " B: " + this.b;
    }

    StateImpl modify(Function<StateImpl, Void> f) {
        f.apply(this);
        return this;
    }

    /*Successor function that generates the list of new states from applying
     * actions to the given state
     * */
    @Override
    public List<ActionStatePair> successor() {
        List<ActionStatePair> a = ActionSingleton.getInstance().stream().map(f -> {
            /* ActionStatePair -> Optional<Pair<Function, StateImpl>>*/
            try {
                return Optional.of(new Pair<>(f, f.apply((StateImpl) this.clone())));
            } catch (CloneNotSupportedException e) {
                /*This will never happen because the types implement Cloneable right?*/
                e.printStackTrace();
                return Optional.empty();
            }
        }).filter(Optional::isPresent).map(Optional::get).filter(_p -> {
            /*Filtered out the empty Optionals, unwrapped them*/
            Pair pair = (Pair<ActionImpl, Optional<StateImpl>>) _p;
            /*ActionImpl returns an Optional so ensure that is present*/
            return ((Optional<StateImpl>) pair.second).isPresent();
        }).map(_p -> {
            Pair pair = (Pair<ActionImpl, Optional<StateImpl>>) _p;
            StateImpl state = ((Optional<StateImpl>) pair.second).get();
            ActionImpl action = ((ActionImpl) pair.first);
            return new ActionStatePair((ActionImpl) ((ActionImpl) action).copy(), state);
        })
                .collect(Collectors.toList());
        CounterSingleton.getInstance().count += a.size();
        return a;
    }
}

