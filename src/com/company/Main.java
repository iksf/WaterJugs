package com.company;

import cm3038.search.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {

        initialiseActions(); /*Sets up program*/
        //   exec(5, 3, 0, 0, 4, -1); /*Execute a search based on parameters passed in here*/
        exec(Optional.empty()); /*Or ask the user for input at runtime*/
    }


    /*real exec takes a List so we'll map the params over to a List then call the exec(List) overload*/
    static private void exec(Integer A_max, Integer B_max, Integer A_0, Integer B_0, Integer A_n, Integer B_n) {
        ArrayList<Pair<String, Integer>> l = new ArrayList<>();
        BiFunction<String, Integer, Void> addL = (k, v) -> {
            l.add(new Pair<>(k, v));
            return null;
        };
        {
            addL.apply("Amax", A_max);
            addL.apply("Bmax", B_max);
            addL.apply("A0", A_0);
            addL.apply("B0", B_0);
            addL.apply("An", A_n);
            addL.apply("Bn", B_n);
        }
        exec(
                Optional.of(l)
        );
    }

    /*Runs a search*/
    static private void exec(Optional<List<Pair<String, Integer>>> maybe_settings) {
        List<Pair<String, Integer>> settings = maybe_settings.orElseGet(Main::get_values); /*If Optional is empty get inputs from user*/
        Function<String, Integer> get = s ->
                settings.stream().filter(pair -> pair.first.equalsIgnoreCase(s)).findFirst().map(pair -> pair.second).get(); /*lambda used later for convenience*/

        /*State initialisation with values from user/passed in*/
        StateImpl initial_state = new StateImpl().modify(state -> {
            state.a = get.apply("A0");   //A0
            state.b = get.apply("B0");   //B0
            state.a_max = get.apply("Amax"); //Amax
            state.b_max = get.apply("Bmax"); //Bmax
            state.a_target = get.apply("An"); //An
            state.b_target = get.apply("Bn"); //Bn
            return null;

        });

        /*Fn to create target state from start state and user input*/
        Function<StateImpl, StateImpl> generate_target = s -> {
            StateImpl state;
            try {
                state = (StateImpl) s.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                state = null;
            }
            assert (state != null);
            state.a = state.a_target;
            state.b = state.b_target;
            return state;
        };
        ProblemImpl problem = new ProblemImpl(
                //Start
                initial_state,
                //Target
                generate_target.apply(initial_state)
        );

        /*Trigger cm3038 libraries search*/
        Path p = problem.search();
        if (p == null) {
            /*No solution*/
            System.out.println("No solution");
        } else {
            /*Print path and cost*/
            p.print();
            System.out.print("Cost = " + p.cost + " Nodes explored = " + CounterSingleton.getInstance().count);
            CounterSingleton.getInstance().reset();
        }
    }

    private static void initialiseActions() {

        /*Define the costs of the actions*/
        Function<Integer, Integer> fill_water_cost = litres -> litres * 2,
                empty_water_cost = litres -> litres * 10,
                move_water_cost = litres -> 1;

        /*This singleton holds all the actions in the program
         cloning the actions is only required for compliance with the 3038 library */
        ActionSingleton.getInstance()
                /*Actions are functions of type StateImpl -> Function<Integer, Integer> -> ActionImpl -> Optional<StateImpl>  */
                /*Empty A*/
                .addAction(state -> cost_fn -> action -> {
                            /*No point considering this option if the vessel is empty*/
                            if (state.a > 0) {
                                /*use passed in function to calculate litres -> cost*/
                                action.cost = cost_fn.apply(state.a);
                                /*empty the jug*/
                                state.a = 0;
                                /*return the new modified state*/
                                return Optional.of(state);
                            }
                            return Optional.empty();
                        },

                        /*Action message*/
                        "Empty A",
                        /*Function<Integer, Integer> used for litres -> cost in closure above */
                        empty_water_cost)

                /*Empty B*/
                .addAction(state -> cost_fn -> action -> {
                    if (state.b > 0) {
                        action.cost = cost_fn.apply(state.b);
                        state.b = 0;
                        return Optional.of(state);
                    }
                    return Optional.empty();
                }, "Empty B", empty_water_cost)


                /* Fill A from "tap" */
                .addAction(state -> cost_fn -> action -> {
                    /*Ignore option if already full*/
                    if (state.a < state.a_max) {
                        /*Filling has a cost l -> 2 * l*/
                        action.cost = cost_fn.apply(state.a_max - state.a);
                        state.a = state.a_max;
                        return Optional.of(state);
                    }
                    return Optional.empty();
                }, "Fill A from source", fill_water_cost)


                /* Fill B from "tap" */
                .addAction(state -> cost_fn -> action -> {
                    if (state.b < state.b_max) {
                        action.cost = cost_fn.apply(state.b_max - state.b);
                        state.b = state.b_max;
                        return Optional.of(state);
                    }
                    return Optional.empty();
                }, "Fill B from source", fill_water_cost)
                /* Fill A from B */
                .addAction(state -> cost_fn -> action -> {
                    /*Total water*/
                    int c = state.a + state.b;
                    /*Is the total water volume more than A can hold?*/
                    if (c > state.a_max) {
                        /*A will be filled */
                        state.a = state.a_max;
                        /*Put remainder in B*/
                        state.b = c - state.a_max;
                    } else {
                        /*A can hold the total water*/
                        state.a += state.b;
                        state.b = 0;
                    }
                    if (state.a <= state.a_max && state.b <= state.b_max) {
                        /*Cost of 1.0*/
                        action.cost = cost_fn.apply(null);
                        return Optional.of(state);
                    }
                    return Optional.empty();
                }, "Fill A from B", move_water_cost)
                /* Fill B from A */
                .addAction(state -> cost_fn -> action -> {
                    int c = state.b + state.a;
                    if (c > state.b_max) {
                        state.b = state.b_max;
                        state.a = c - state.b_max;
                    } else {
                        state.b += state.a;
                        state.a = 0;
                    }
                    if (state.b <= state.b_max && state.a <= state.a_max) {
                        action.cost = cost_fn.apply(null);
                        return Optional.of(state);
                    }
                    return Optional.empty();

                }, "Fill B from A", move_water_cost);
    }

    /*Responsible for ensuring a settings list is populated*/
    private static List<Pair<String, Integer>> get_values() {
        List<Pair<String, Integer>> mins = new ArrayList<>();
        {
            BiFunction<String, Integer, Void> addToMins = (k, v) -> {
                mins.add(new Pair<>(k, v));
                return null;
            };
            {
                addToMins.apply("Amax", 1);
                addToMins.apply("Bmax", 1);
                addToMins.apply("A0", 0);
                addToMins.apply("B0", 0);
                addToMins.apply("An", -1);
                addToMins.apply("Bn", -1);
            }
        }
        /*Get user inputs
         * Generate a BufferedReader
         * */
        System.out.println("Enter inputs (An and Bn can be -1 for any value)");
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        /*execute this function on each element in the list and map their outputs to a list*/
        return mins.stream().map(pair -> {
            String key = pair.first;
            /*This function prompts for input for a key and gets input from the user*/
            Function<Void, Optional<Integer>> get_input = _v -> {
                System.out.println(key);
                String input_string;
                try {
                    input_string = buffer.readLine();
                } catch (IOException e) {
                    /*Invalid input*/
                    System.out.println("Invalid input");
                    /*Empty optional means retry */
                    return Optional.empty();
                }
                /*Check for empty input*/
                if (input_string == null || input_string.isEmpty()) return Optional.empty();
                Integer input_int;
                try {
                    input_int = Integer.valueOf(input_string);
                } catch (NumberFormatException e) {
                    /*Probably non-numerical string entered*/
                    e.printStackTrace();
                    return Optional.empty();
                }
                /*Return the value for this setting if greater than or eq min*/
                return input_int >= pair.second ? Optional.of(input_int) : Optional.empty();
            };
            Optional<Integer> input = Optional.empty();
            /*Loop forever until valid input is entered*/
            while (input.isEmpty()) {
                Optional<Integer> i = get_input.apply(null);
                if (i.isPresent()) {
                    Integer kt = i.get();
                    if (kt >= pair.second) {
                        input = i;
                    }
                }
            }
            Integer input_int = input.get();
            return new Pair<>(pair.first, input_int);
        }).collect(Collectors.toList());
    }
}
