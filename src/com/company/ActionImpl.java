package com.company;

import cm3038.search.Action;

import java.util.Optional;
import java.util.function.Function;

public class ActionImpl extends Action implements Function<StateImpl, Optional<StateImpl>>, Cloneable {
    private String action_string;
    private Function<StateImpl, Function<Function<Integer, Integer>, Function<ActionImpl, Optional<StateImpl>>>> f;
    private Function<Integer, Integer> cost_fn;

    ActionImpl copy() {
        try {
            return (ActionImpl) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    ActionImpl(Function<StateImpl, Function<Function<Integer, Integer>, Function<ActionImpl, Optional<StateImpl>>>> f,
               String action_string, Function<Integer, Integer> cost_fn) {
        this.f = f;
        this.cost_fn = cost_fn;
        this.action_string = action_string;
    }

    @Override
    public Optional<StateImpl> apply(StateImpl state) {
        return this.f.apply(state).apply(cost_fn).apply(this);
    }

    @Override
    public String toString() {
        return this.action_string + " cost: " + this.cost;
    }
}
