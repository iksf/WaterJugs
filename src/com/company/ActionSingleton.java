package com.company;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/*
This is a singleton (a class only permitted one instantiated object at a time)
The singleton is used to hold the actions for each run
* */
class ActionSingleton {
    private static ActionSingleton ourInstance = new ActionSingleton();
    private List<ActionImpl> actionList = new ArrayList<>();

    private ActionSingleton() {
    }

    static ActionSingleton getInstance() {
        return ourInstance;
    }

    /*Add to the list of actions we apply to the states to generate successors*/
    ActionSingleton addAction(Function<StateImpl, Function<Function<Integer, Integer>, Function<ActionImpl, Optional<StateImpl>>>> f,
                              String action_message, Function<Integer, Integer> cost_fn) {
        this.actionList.add(new ActionImpl(f, action_message, cost_fn));
        return this;
    }

    Stream<ActionImpl> stream() {
        return this.actionList.stream();
    }
}
