package com.company;

import cm3038.search.Node;
import cm3038.search.State;
import cm3038.search.informed.BestFirstSearchProblem;

import java.util.Optional;

public class ProblemImpl extends BestFirstSearchProblem {
    ProblemImpl(State start, State goal) {
        super(start, goal);
    }


    @Override
    public double evaluation(Node node) {
        StateImpl state = (StateImpl) node.state;
        /*Heuristic function
         *
         * Abs ensures value is positive. node.getCost() gets cost of the path so far
         *
         * difference between target and value is minimised, cost of the minimum potential path is always 1 per node
         * because move between jugs only costs 1 regardless of amount -> best solution involves only that move
         *
         * if state.b_target is 0 I ignore the value for the B jug
         * */

        Optional<Integer> at = state.a_target != -1 ? Optional.of(state.a_target) : Optional.empty();
        Optional<Integer> bt = state.b_target != -1 ? Optional.of(state.b_target) : Optional.empty();

        //
        // return /*g(n)*/node.getCost() +/*h(n)*/ (((double) Math.abs(state.a - state.a_target) + state.b_target != 0 ?
        //Math.abs(state.b - state.b_target) : 0)) / (double) (state.a_max + state.b_max);
        //

        /*
        f(n) = g(n) + ( |A -An| + |B-Bn| ) / (An + Bn + 1)
         */
        return/*g(n)*/node.getCost() +/*h(n)*/(double) (Math.abs(at.map(integer -> state.a - integer).orElse(0)) +
                Math.abs(bt.map(value -> state.b - value).orElse(0))) / (double) ((at.orElse(0)) + (bt.orElse(0)) + 1);
    }

    @Override
    public boolean isGoal(State s) {
        StateImpl state = (StateImpl) s;
        return (state.a == state.a_target || state.a_target == -1) && (state.b == state.b_target || state.b_target == -1);
    }
}
