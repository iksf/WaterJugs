package com.company;
/*Generic Pair class*/
class Pair<T, U> {
    final T first;
    final U second;

    Pair(T first, U second) {
        this.first = first;
        this.second = second;
    }
}
