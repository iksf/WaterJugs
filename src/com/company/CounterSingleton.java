package com.company;

public class CounterSingleton {
    int count = 0;
    private static CounterSingleton ourInstance = new CounterSingleton();

    public static CounterSingleton getInstance() {
        return ourInstance;
    }
    void reset() {
        this.count= 0;
    }

    private CounterSingleton() {
    }
}
